const apiurl = "https://www.fip.fr/latest/api/graphql?operationName=Now&variables={%22stationId%22%3A7%2C%22previousTrackLimit%22%3A1}&extensions={%22persistedQuery%22%3A{%22version%22%3A1%2C%22sha256Hash%22%3A%228a931c7d177ff69709a79f4c213bd2403f0c11836c560bc22da55628d8100df8%22}}";
const placeholder = "https://www.fip.fr/static/media/placeholder_artwork.2c5c7880.png";
const altplaceholder = "Images/fip-logo.png";
const extratime = 1000;

var debug = false;
var announceflag = false;
var currentTrackInfo = {};
var reloadtimer;
var displayFlag = true;

var nohttps = false;
var usecssshadow = false;

function load() {
    dashcode.setupParts();
    getPrefs();
}

function remove() {
    killTimer();
}

function hide() {
    killTimer();
}

function show() {
    getTrackInfo();
}

function sync() {

}

function showBack(event) {
    killTimer();
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

function showFront(event) {
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
    getTrackInfo();
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}

getTrackInfo();

function getTrackInfo() {
    var fpx = new XMLHttpRequest();
    fpx.open("GET", apiurl);
    fpx.send();
    fpx.onload = function() {
        if (fpx.status != 200) {
            if (debug) {    
                alert("FIP: Error " + fpx.status+ " : " + fpx.statusText);
                killTimer();
                parseFailure();
            }
        } else {
            if (debug) {
                alert("FIP: Done, got " + fpx.response.length + " bytes");
            }
            var trackinfo = eval("(" + fpx.responseText + ")");
            if (debug) {
            		alert(fpx.responseText);
                alert(trackinfo);
            }
            parseTrackInfo(trackinfo);
            if (debug) {
                alert(currentTrackInfo);
            }
            if (displayFlag) applyTrackInfo();
            reloadtimer = setTimeout(getTrackInfo, currentTrackInfo.nextTimer);
        }
    }
    fpx.onerror = function() {
        if (debug) {
            alert("FIP: Request failed");
        }
        killTimer();
        parseFailure();
    }
}

function killTimer() {
    if (reloadtimer) {
        clearTimeout(reloadtimer);
        reloadtimer = null;
    }
    if (debug) {
        alert("Timer stopped!");
    }
}

function parseTrackInfo(trackinfo) {
	displayFlag = true;
	currentTrackInfo = {};
	currentTrackInfo.last = {};
	currentTrackInfo.now = {};
	currentTrackInfo.next = {};
	announceflag = false;
	if ((trackinfo.data.now.playing_item.title) && (!trackinfo.data.now.playing_item.subtitle) && (trackinfo.data.nextTracks.length == 0)) {
		announceflag = true;
		if (!trackinfo.data.nextTracks) {
			announceflag = false;
			parseFailure();
		} else {
      var t = new Date();
			currentTrackInfo.now.time = t.toString().split(" ")[4].substr(0, 5);
			currentTrackInfo.now.cover = altplaceholder;
			currentTrackInfo.now.title = trackinfo.data.now.playing_item.title;
			currentTrackInfo.now.artist = "Event broadcast or announcement";
			currentTrackInfo.now.album = "Refreshes every 30 seconds";
			currentTrackInfo.next.time = currentTrackInfo.now.time;
			currentTrackInfo.next.cover = placeholder;
			currentTrackInfo.next.title = "No info available";
			currentTrackInfo.next.artist = "Please check back later";
      currentTrackInfo.nextTimer = 30000;
		}
	} else {
		var d = new Date;
		if (debug) {
			alert("Date: " + d.getTime());
			alert("Next Refresh: " + trackinfo.data.now.next_refresh * 1000);
		}
		if (typeof trackinfo.data.now.next_refresh !== "undefined") {
			currentTrackInfo.nextTimer = (trackinfo.data.now.next_refresh * 1000) - d.getTime() + extratime;
		} else {
			currentTrackInfo.nextTimer = 1000;
		}
		if (debug) {
			alert("Original Next Refresh: " + currentTrackInfo.nextTimer);
		}
		if (currentTrackInfo.nextTimer < 0) {
			displayFlag = false;
			currentTrackInfo.nextTimer = 1000;
        }
		if (debug) {
			alert("Next update in " + (currentTrackInfo.nextTimer / 1000) + " seconds!");
		}
		if (trackinfo.data.now.playing_item.start_time) {
			t = new Date(trackinfo.data.now.playing_item.start_time * 1000);
			currentTrackInfo.now.time = t.toString().split(" ")[4].substr(0, 5);
		} else {
			currentTrackInfo.now.time = "00:00";
		}
		if ((typeof trackinfo.data.now.playing_item.cover === "undefined") || (trackinfo.data.now.playing_item.cover == "")) {
			currentTrackInfo.now.cover = altplaceholder;
		} else {
			currentTrackInfo.now.cover = trackinfo.data.now.playing_item.cover;
		}
		if ((typeof trackinfo.data.now.playing_item.title === "undefined") || (trackinfo.data.now.playing_item.title == "")) {
			currentTrackInfo.now.artist = "--";
		} else {
			currentTrackInfo.now.artist = trackinfo.data.now.playing_item.title;
		}
		if ((typeof trackinfo.data.now.playing_item.subtitle === "undefined") || (trackinfo.data.now.playing_item.subtitle == "")) {
			currentTrackInfo.now.title = "--";
		} else {
			currentTrackInfo.now.title = trackinfo.data.now.playing_item.subtitle;
		}
		if ((typeof trackinfo.data.now.song.album === "undefined") || (trackinfo.data.now.song.album == "")) {
			currentTrackInfo.now.album = "--";
		} else {
			currentTrackInfo.now.album = trackinfo.data.now.song.album;
		}
		if (trackinfo.data.nextTracks.length > 0) {
			if (typeof trackinfo.data.nextTracks[0].start_time !== "undefined") {
				t = new Date(trackinfo.data.nextTracks[0].start_time * 1000);
				currentTrackInfo.next.time = t.toString().split(" ")[4].substr(0, 5);
			} else {
				currentTrackInfo.next.time = "00:00";
			}
			if ((typeof trackinfo.data.nextTracks[0].cover === "undefined") || (trackinfo.data.nextTracks[0].cover == "")) {
				currentTrackInfo.next.cover = placeholder;
			} else {
				currentTrackInfo.next.cover = trackinfo.data.nextTracks[0].cover;
			}
			if ((typeof trackinfo.data.nextTracks[0].title === "undefined") || (trackinfo.data.nextTracks[0].title == "")) {
				currentTrackInfo.next.artist = "--";
			} else {
				currentTrackInfo.next.artist = trackinfo.data.nextTracks[0].title;
			}
			if ((typeof trackinfo.data.nextTracks[0].subtitle === "undefined") || (trackinfo.data.nextTracks[0].subtitle == "")) {
				currentTrackInfo.next.title = "--";
			} else {
				currentTrackInfo.next.title = trackinfo.data.nextTracks[0].subtitle;
			}
		} else {
			currentTrackInfo.next.time = currentTrackInfo.now.time;
			currentTrackInfo.next.cover = placeholder;
			currentTrackInfo.next.title = "No info available";
			currentTrackInfo.next.artist = "Please check back later";
		}
	}
	if (typeof trackinfo.data.previousTracks.edges[0].node.start_time !== "undefined") {
		var t = new Date(trackinfo.data.previousTracks.edges[0].node.start_time * 1000);
		currentTrackInfo.last.time = t.toString().split(" ")[4].substr(0, 5);
	} else {
		currentTrackInfo.last.time = "00:00";
	}
	if ((typeof trackinfo.data.previousTracks.edges[0].node.cover === "undefined") || (trackinfo.data.previousTracks.edges[0].node.cover == "")) {
		currentTrackInfo.last.cover = placeholder;
	} else {
		currentTrackInfo.last.cover = trackinfo.data.previousTracks.edges[0].node.cover;
	}
	if ((typeof trackinfo.data.previousTracks.edges[0].node.title === "undefined") || (trackinfo.data.previousTracks.edges[0].node.title == "")) {
		currentTrackInfo.last.artist = "--";
	} else {
		currentTrackInfo.last.artist = trackinfo.data.previousTracks.edges[0].node.title;
	}
	if ((typeof trackinfo.data.previousTracks.edges[0].node.subtitle === "undefined") || (trackinfo.data.previousTracks.edges[0].node.subtitle == "")) {
		currentTrackInfo.last.title = "--";
	} else {
		currentTrackInfo.last.title = trackinfo.data.previousTracks.edges[0].node.subtitle;
	}
	if (debug) {
		alert(":LAST:");
		alert("last.time: " + currentTrackInfo.last.time);
		alert("last.cover: " + currentTrackInfo.last.cover);
		alert("last.artist: " + currentTrackInfo.last.artist);
		alert("last.title: " + currentTrackInfo.last.title);
		alert("::");
		alert(":NOW:");
		alert("now.time: " + currentTrackInfo.now.time);
		alert("now.cover: " + currentTrackInfo.now.cover);
		alert("now.artist: " + currentTrackInfo.now.artist);
		alert("now.title: " + currentTrackInfo.now.title);
		alert("now.album: " + currentTrackInfo.now.album);
		alert("::");
		alert(":NEXT:");
		alert("next.time: " + currentTrackInfo.next.time);
		alert("next.cover: " + currentTrackInfo.next.cover);
		alert("next.artist: " + currentTrackInfo.next.artist);
		alert("next.title: " + currentTrackInfo.next.title);
		alert("::");
	}
}

function parseFailure() {
    currentTrackInfo = {};
    currentTrackInfo.last = {};
    currentTrackInfo.now = {};
    currentTrackInfo.next = {};
    currentTrackInfo.nextTimer = 10000;
    currentTrackInfo.last.time = "00:00";
    currentTrackInfo.last.cover = placeholder;
    currentTrackInfo.last.artist = "--";
    currentTrackInfo.last.title = "--";
    currentTrackInfo.now.time = "00:00";
    currentTrackInfo.now.cover = altplaceholder;
    currentTrackInfo.now.artist = "Please stand by!";
    currentTrackInfo.now.title = "Could not get current info";
    currentTrackInfo.now.album = "Reloading in 10 seconds";
    currentTrackInfo.last.time = "00:00";
    currentTrackInfo.next.cover = placeholder;
    currentTrackInfo.next.artist = "--";
    currentTrackInfo.next.title = "--";
}

function applyTrackInfo() {
    if (!displayFlag) return;
    if (currentTrackInfo.last.cover == placeholder) {
        document.getElementById("now-cover-img").src = placeholder;
    } else {
        setImageURL(currentTrackInfo.last.cover, "last-cover-img");
    }
    document.getElementById("last-time").innerText = currentTrackInfo.last.time;
    document.getElementById("last-title").innerText = currentTrackInfo.last.title;
    document.getElementById("last-artist").innerText = currentTrackInfo.last.artist;
    if ((currentTrackInfo.now.cover == placeholder) || (currentTrackInfo.now.cover == altplaceholder)) {
        document.getElementById("now-cover-img").src = currentTrackInfo.now.cover;
    } else {
        setImageURL(currentTrackInfo.now.cover, "now-cover-img");
    }
    document.getElementById("now-time").innerText = currentTrackInfo.now.time;
    document.getElementById("now-title").innerText = currentTrackInfo.now.title;
    document.getElementById("now-artist").innerText = currentTrackInfo.now.artist;
    document.getElementById("now-album").innerText = currentTrackInfo.now.album;
    if (currentTrackInfo.next.cover == placeholder) {
        document.getElementById("next-cover-img").src = placeholder;
    } else {
        setImageURL(currentTrackInfo.next.cover, "next-cover-img");
    }
    document.getElementById("next-time").innerText = currentTrackInfo.next.time;
    document.getElementById("next-title").innerText = currentTrackInfo.next.title;
    document.getElementById("next-artist").innerText = currentTrackInfo.next.artist;
}

function setImageURL(url, imgid) {
    if (url == null) {
        if (imgid == "now-cover-img") {
            document.getElementById(imgid).src = altplaceholder;
        } else {
            document.getElementById(imgid).src = placeholder;
        }
    } else {
        url = url.replace("1000x1000-", "64x64-");
        if (nohttps) {
            url = url.replace("https://", "http://");
        }
        if (debug) {
            alert("MODIFIED Image URL: " + url);
        }
        var imx = new XMLHttpRequest();
        imx.open("GET", url, true);
        imx.overrideMimeType('text/plain; charset=x-user-defined');
        imx.send(null);
        imx.onload = function() {
            if (imx.status != 200) {
                if (debug) {    
                    alert("FIP Image: Error " + imx.status+ " : " + imx.statusText);
                    alert("Image URL " + url + " could not be loaded, inserting alternative.");
                }
                document.getElementById(imgid).src = placeholder;
            } else {
                if (debug) {
                    alert("FIP Image: Done, got " + imx.response.length + " bytes");
                }
                binary = "";
                for (i = 0; i < imx.responseText.length; i++) {
                    binary += String.fromCharCode(imx.responseText.charCodeAt(i) & 0xff);
                }
                var img = document.getElementById(imgid);
                img.src = "data:image/jpeg;base64," + btoa(binary);
            }
        }
        imx.onerror = function() {
            if (debug) {
                alert("FIP Image: Request failed");
            }
            document.getElementById(imgid).src = placeholder;
        }
    }
}

function getPrefs() {
    if (window.widget) {
        if (typeof widget.preferenceForKey("useCSSShadow") === "undefined") {
            widget.setPreferenceForKey(usecssshadow, "useCSSShadow");
            document.getElementById("shadow-input").checked = usecssshadow;
        }
        else {
            usecssshadow = widget.preferenceForKey("useCSSShadow");
            document.getElementById("shadow-input").checked = usecssshadow;
        }
        if (debug) {
            alert("usecssshadow: " + usecssshadow);
        }
        handleShadow();
        if (typeof widget.preferenceForKey("noHTTPSforImages") === "undefined") {
            widget.setPreferenceForKey(nohttps, "noHTTPSforImages");
            document.getElementById("https-input").checked = nohttps;
        }
        else {
            nohttps = widget.preferenceForKey("noHTTPSforImages");
            document.getElementById("https-input").checked = nohttps;
        }
        if (debug) {
            alert("nohttps: " + nohttps);
        }
    }
}

function useCSSShadowCheckBoxClicked(event) {
	usecssshadow = document.getElementById("shadow-input").checked ;
    if (window.widget) {
        widget.setPreferenceForKey(usecssshadow, "useCSSShadow");
    }
    handleShadow();
}

function noHTTPSCheckBoxClicked(event) {
	nohttps = document.getElementById("https-input").checked;
    if (window.widget) {
        widget.setPreferenceForKey(nohttps, "noHTTPSforImages");
    }
}

function openWebsite(event) {
    var theURL = "https://fip.fr";
    widget.openURL(theURL);
}

function handleShadow() {
    if (usecssshadow) {
        document.getElementById("front-shadow").style.display = "none";
        document.getElementById("back-shadow").style.display = "none";
        document.getElementById("front-image").setAttribute("style","-webkit-filter: drop-shadow(0px 0px 8px #000000);");
        document.getElementById("front-image").setAttribute("style","filter: drop-shadow(0px 0px 8px #000000);");
        document.getElementById("back-image").setAttribute("style","-webkit-filter: drop-shadow(0px 0px 8px #000000);");
        document.getElementById("back-image").setAttribute("style","filter: drop-shadow(0px 0px 8px #000000);");
    } else {
        document.getElementById("front-shadow").style.display = "";
        document.getElementById("back-shadow").style.display = "";
        document.getElementById("front-image").setAttribute("style","-webkit-filter: drop-shadow(0px 0px 0px #000000);");
        document.getElementById("front-image").setAttribute("style","filter: drop-shadow(0px 0px 0px #000000);");
        document.getElementById("back-image").setAttribute("style","-webkit-filter: drop-shadow(0px 0px 0px #000000);");
        document.getElementById("back-image").setAttribute("style","filter: drop-shadow(0px 0px 0px #000000);");
    }
}
