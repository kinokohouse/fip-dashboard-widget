# FIP Now Playing #
A Dashboard Widget to check what the title of that cool song was just now on Radio FIP France. MIT License. Ready to use binary in the Downloads section.

The widget should speak for itself, but on the back there are two options:

* Older versions of macOS do not support CSS shadows for widgets, so a 'shadow layer' is supplied to provide a contrasting rim around the irregularly shaped widget. If you are on a newer version of macOS, the CCS shadow will look a little bit cleaner around the edges (although a bit more pronounced).
* If you notice on older versions of macOS that album images are consistently missing when you think they really should be there, try checking the 'No HTTPS for images' option. This option is a bit hit-and-miss (depending on the CDN from which the images are downloaded), and will probably not remain working forever.

Tested under macOS 10.6.8 Snow Leopard and 10.13.6 High Sierra, but should work on everything since macOS 10.4.3.


Building
--------
Editing and deploying can be done from either version of DashCode, or you can
edit the files within the widget bundle with your favourite text/image editors. 

Version info
------------
Current version is 1.4.

### Changes from 1.3 ###
* Quicker loading of new information, and will load only the newest information available
(so no flashes of old songs in between while you're waiting for the info to refresh).

### Changes from 1.2 ###
* Fixed script breaking on null image.

### Changes from 1.1 ###
* This version **_really_** handles announcements (and live events) correctly.

### Changes from 1.0 ###
* Exhaustive checking for null objects;
* This version should handle announcements between songs correctly.
